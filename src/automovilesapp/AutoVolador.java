/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package automovilesapp;

import java.util.Date;

/**
 *
 * @author Jgoyes 
 * @Class AutoVolador
 * @version 1.0
 * Clase autovolador que hereda de de la clase carro para su implementación 
 */
public class AutoVolador extends Automovil {
    public boolean esta_Volando=false;
    
    //metodos constructores 

    public AutoVolador(String color, String marca, Date anioDate, boolean pesado) {
        super(color, marca, anioDate, pesado);
    }

    public AutoVolador() {
    }
    
    //metodos getters and setters 

    public boolean isEsta_Volando() {
        return esta_Volando;
    }

    public void setEsta_Volando(boolean esta_Volando) {
        this.esta_Volando = esta_Volando;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Date getAnioDate() {
        return anioDate;
    }

    public void setAnioDate(Date anioDate) {
        this.anioDate = anioDate;
    }

    public boolean isPesado() {
        return pesado;
    }

    public void setPesado(boolean pesado) {
        this.pesado = pesado;
    }
    
    
    
}
