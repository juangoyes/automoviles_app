/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package automovilesapp;

import java.util.Date;

/**
 *
 * @author JGoyes
 * @Class  Automovil
 * @version 1.0
 * Clase automovil 
 */
public class Automovil {
 public String color;
 public String marca;
 public Date anioDate;
 public boolean pesado;
    
    //Contructor con atributos  
    public Automovil(String color, String marca, Date anioDate, boolean pesado) {
        this.color = color;
        this.marca = marca;
        this.anioDate = anioDate;
        this.pesado = pesado;
    }
    //constructor vacio

    public Automovil() {
    }
    
    //metodos getters and setters 

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Date getAnioDate() {
        return anioDate;
    }

    public void setAnioDate(Date anioDate) {
        this.anioDate = anioDate;
    }

    public boolean isPesado() {
        return pesado;
    }

    public void setPesado(boolean pesado) {
        this.pesado = pesado;
    }
    
    
}

